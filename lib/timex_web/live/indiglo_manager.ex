defmodule TimexWeb.IndigloManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {:ok, %{ui_pid: ui, st: :indiglo_off, count: 0}}
    end

    def handle_info(:"top-right", %{ui_pid: ui, st: :indiglo_off} = state) do
        GenServer.cast(ui, :set_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_on)}
    end

    def handle_info(:"top-right", %{ui_pid: _ui, st: :indiglo_on} = state) do
        Process.send_after(self(), :tick, 2000)
        {:noreply, state |> Map.put(:st, :waiting)}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :waiting} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_off)}
    end

    def handle_info(:"top-right", %{ui_pid: ui, st: :waiting} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_Off)}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :indiglo_on} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:count, 0) |> Map.put(:st, :alarm_off)}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :waiting} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:count, 0) |> Map.put(:st, :alarm_off)}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :indiglo_off} = state) do
        GenServer.cast(ui, :set_indiglo)
        {:noreply, state |> Map.put(:count, 1) |> Map.put(:st, :alarm_on)}
    end

    def handle_info(%{ui_pid: _ui, st: :alarm_on} = state) do
        Process.send_after(self(), :tick, 1000)
        {:noreply, state}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :alarm_on, count: count} = state) do
        GenServer.cast(ui, :unset_indiglo)
        count + 1
        {:noreply, state |> Map.put(:st, :alarm_off) |> Map.put(:count, count)}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :alarm_on, count: 9} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_off)}
    end

    def handle_info(%{ui_pid: _ui, st: :alarm_off} = state) do
        Process.send_after(self(), :tick, 1000)
        {:noreply, state}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :alarm_off, count: count} = state) do
        GenServer.cast(ui, :unset_indiglo)
        count + 1
        {:noreply, state |> Map.put(:st, :alarm_on) |> Map.put(:count, count)}
    end

    #No me acorde de como implementar la funcion de fallback

    def handle_info(_event, state) do
        {:noreply, state}
    end

end
