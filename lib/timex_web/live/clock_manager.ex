defmodule TimexWeb.ClockManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {_, now} = :calendar.local_time()

        Process.send_after(self(), :tick, 1000)

        {
            :ok,
            %{
                ui_pid: ui,
                time: Time.from_erl!(now),
                # EXAM: NEW STATE VARIABLE
                alarm_time: Time.from_erl!(now) |> Time.add(60),
                mode: :time
            }
        }
    end

    def handle_info(:tick, %{ui_pid: ui, time: time, mode: mode, alarm_time: alarm_time} = state) do
        Process.send_after(self(), :tick, 1000)

        time = Time.add(time,1)

        if mode == :time do
            GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
        end

        # EXAM: "WHEN" TRANSITION
        if Time.truncate(time, :second) == Time.truncate(alarm_time, :second) do
            :gproc.send({:p, :l, :ui_event}, :alarm)
        end

        {:noreply, state |> Map.put(:time, time) }
    end

    def handle_info(:"top-left", %{ui_pid: ui, mode: :time} = state) do
        IO.inspect Map.get(state, :mode)
        {:noreply, state |> Map.put(:mode, :chrono) }
    end
    def handle_info(:"top-left", %{ui_pid: ui, mode: :chrono} = state) do
        IO.inspect Map.get(state, :mode)
        {:noreply, state |> Map.put(:mode, :time) }
    end

    def handle_info(event, state), do: {:noreply, state}
end
